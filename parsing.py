import requests
from bs4 import BeautifulSoup


url = 'https://vfrsute.ru/%D1%81%D0%BA%D0%B0%D0%BD%D0%B2%D0%BE%D1%80%D0%B4' \
      '/%D1%81%D0%BB%D0%BE%D0%B2%D0%BE-%D0%B8%D0%B7-10-%D0%B1%D1%83%D0%BA%D0%B2/?'
headers = {'accept': '*/*',
           'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 '
                         '(HTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36'}


def get_html(url, params=None):
    # Подключаемся к сайту.
    r = requests.get(url=url, params=params, headers=headers)
    return r


fr = get_html(url, headers)
if fr.status_code == 200:
    print('Удачное подключение')
else:
    print('Данные не получены.')

